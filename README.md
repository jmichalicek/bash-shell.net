[![Codeship Status for jmichalicek/bash-shell.net](https://app.codeship.com/projects/26e5e670-202c-0137-8746-62ccfb61d2cc/status?branch=master)](https://app.codeship.com/projects/329379)

# bash-shell.net
Main project for http://bash-shell.net/


# Tools

## Frameworks
* Django
* Bootstrap 4

## Sites
* https://colormind.io
* https://www.cssfontstack.com/Calibri

## CSS and Icons
* https://simpleicons.org/
* https://bootswatch.com/
